#!/bin/sh
echo "Ubuntu: Installing Mongo DB!\n\n"

# Ubuntu ensures the authenticity of software packages by verifying that they are signed with GPG keys, so we first have to import they key for the official MongoDB repository.
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927

# add the MongoDB repository details so apt will know where to download the packages from.
sudo echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

# After adding the repository details, we need to update the packages list.
sudo apt-get update

# install the MongoDB package itself.
sudo apt-get install -y mongodb-org

# Start MongoDB
sudo systemctl start mongod

# Check status
sudo systemctl status mongod

# enable automatically starting MongoDB when the system starts.
sudo systemctl enable mongod