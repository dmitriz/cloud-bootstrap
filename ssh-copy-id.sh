# Enable login without password
echo "Usage: ./ssh-copy-id <IP address>"
echo "Attempting to store your public key remotely for password-free login..."
ssh-copy-id -i ~/.ssh/id_rsa.pub root@$1
