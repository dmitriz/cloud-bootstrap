#!/bin/sh
echo "Ubuntu: updating and upgrading!\n\n"
sudo apt-get update -y 
sudo apt-get upgrade -y

# nodejs 8
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# postgres
sudo apt-get install -y postgresql postgresql-contrib


# mongodb
# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
# echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
# sudo apt-get update
# sudo apt-get install -y mongodb-org


# mkdir /data
# mkdir /data/db
# apt install -y mongodb-server

