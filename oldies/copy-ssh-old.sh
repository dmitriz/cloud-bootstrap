# usage: copy-ssh.sh IP_ADDR
remote="root@$1"
ddir=".ssh"
sourcefile="id_rsa.pub"
targetfile="authorized_keys"
ssh "$remote" "mkdir -p $ddir" && scp -r "$ddir/$sourcefile" "$remote:$ddir/$targetfile"
